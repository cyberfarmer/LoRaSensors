<!DOCTYPE html> 
<html> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radio Server Data</title>
    <link rel="stylesheet" href="simple.min.css">
</head>
<body> 
        <header>
        <h1>Radio Server Data</h1>
        </header>
        </center>
        <hr>
        <table>
        
        <tr>
                <th style='text-align:center'>Date</th>
                <th style='text-align:center'>Time Received</th>
                <th style='text-align:center'>Client ID</th>
                <th style='text-align:center'>Humidity</th>
                <th style='text-align:center'>Temperature</th>
        </tr>
  
        <?php
        // Open a file 
        $file = fopen("/home/pi/LoRaSensors/RadioServer/db.csv", "r"); 
  
        // Fetching data from csv file row by row 
        while (($data = fgetcsv($file)) !== false) { 
  
            // HTML tag for placing in row format 
            echo "<tr>"; 
            foreach ($data as $i) { 
                echo "<td style='text-align:center'>" . htmlspecialchars($i)  
                    . "</td>"; 
            } 
            echo "</tr> \n"; 
        } 
  
        // Closing the file 
        fclose($file); 
        ?> 
        </table>
    </center> 
</body> 
  
</html>
