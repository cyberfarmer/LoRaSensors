#!/bin/bash

haveProg() { # Checks if a program is installed
    [ -x "$(which $1)" ]
}

func_apt() { # Installs lighttpd on Debian based OS'
    apt update
    apt install lighttpd -y
}

func_yum() { # Installs lighttpd on RHEL based OS'
    yum install lighttpd -y
}

func_pacman() { # Installs lighttpd on Arch based OS'
    yes | pacman -Sy lighttpd
}

func_zypper() { # Installs lighttpd on SUSE based OS'
    yes | zypper install lighttpd
}

# Checks if the script is being run as root
if [ `whoami` != root ]; then
	echo "Run this script as root or with sudo"
	exit
fi

# Installs and starts lighttpd
if haveProg apt ; then func_apt
elif haveProg yum ; then func_yum
elif haveProg pacman ; then func_pacman
elif haveProg zypper ; then func_zypper
else
    echo 'OS not supported, try manually installing an http server'
    exit
fi
systemctl restart lighttpd
systemctl enable lighttpd

# Prepping board so the server works
pip3 install --upgrade setuptools
pip3 install --upgrade adafruit-python-shell
pip3 install --upgrade Adafruit-PlatformDetect
pip3 install --upgrade adafruit-circuitpython-ssd1306
pip3 install --upgrade adafruit-circuitpython-framebuf
pip3 install --upgrade adafruit-circuitpython-rfm9x

raspi-config nonint do_i2c 0
raspi-config nonint do_spi 0
raspi-config nonint do_serial 0
raspi-config nonint do_ssh 0
raspi-config nonint do_camera 0
raspi-config nonint disable_raspi_config_at_boot 0

apt install -y i2c-tools
pip3 install --upgrade RPi.GPIO
pip3 install --upgrade adafruit-blinka

# Sets up needed files
touch /home/pi/LoRaSensors/RadioServer/db.csv
chown pi:pi /home/pi/LoRaSensors
chmod +r /home/pi/LoRaSensors/RadioServer/db.csv
chown pi:pi /home/pi/LoRaSensors/RadioServer/db.csv
chown pi:pi /home/pi/LoRaSensors/RadioServer/font5x8.bin
mv index.php /var/www/html/
mv LoRaSensors.service /etc/systemd/system/LoRaSensors.service

# Asks if user wants server enabled on startup
systemctl daemon-reload
read -r -p "Would you like to enable the LoRa Server on reboot? [Y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        systemctl start LoRaSensors
        systemctl enable LoRaSensors
        ;;
    *)
        systemctl start LoRaSensors
        ;;
esac

# Prints IP address for the webserver
IP=$(ip -4 addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
echo "Reboot is needed for server to work, run sudo reboot"
echo "Visit $IP in a browser to view data after reboot"
