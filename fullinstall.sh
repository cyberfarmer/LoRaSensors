#!/bin/bash

haveProg() { # Checks if a program is installed
    [ -x "$(which $1)" ]
}

func_apt() { # Installs git on Debian based OS'
    apt update
    apt install git -y
}

func_yum() { # Installs git on RHEL based OS'
    yum install git -y
}

func_pacman() { # Installs git on Arch based OS'
    yes | pacman -Sy git
}

func_zypper() { # Installs git on SUSE based OS'
    yes | zypper install git
}

# Checks if the script is being run as root
if [ `whoami` != root ]; then
	echo "Run this script as root or with sudo"
	exit
fi

# Checks if git is installed, if not tries to install it
if haveProg git 
then
    echo "Git already installed, skipping"
else
    if haveProg apt ; then func_apt
    elif haveProg yum ; then func_yum
    elif haveProg pacman ; then func_pacman
    elif haveProg zypper ; then func_zypper
    else
        echo 'OS not supported, try manually installing git then rerun this script.'
        exit
    fi
fi

# Clones repo
cd /home/pi/
git clone https://codeberg.org/cyberfarmer/LoRaSensors
cd LoRaSensors/RadioServer/
bash localinstall.sh

# Clean up
rm -r /home/pi/LoRaSensors/RadioClient/
rm /home/pi/LoRaSensors/RadioServer/localinstall.sh
