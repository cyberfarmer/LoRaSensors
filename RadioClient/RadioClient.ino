#include <SPI.h>
#include <RH_RF95.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>
#include "Adafruit_Si7021.h"
#include <avr/dtostrf.h>
#include <ArduinoUniqueID.h>

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

#if defined(ESP8266)
    /* for ESP w/featherwing */ 
    #define RFM95_CS  2    // "E"
    #define RFM95_RST 16   // "D"
    #define RFM95_INT 15   // "B"

#elif defined(ESP32)  
    /* ESP32 feather w/wing */
    #define RFM95_RST     27   // "A"
    #define RFM95_CS      33   // "B"
    #define RFM95_INT     12   //  next to A

#elif defined(NRF52)  
    /* nRF52832 feather w/wing */
    #define RFM95_RST     7   // "A"
    #define RFM95_CS      11   // "B"
    #define RFM95_INT     31   // "C"
  
#elif defined(TEENSYDUINO)
    /* Teensy 3.x w/wing */
    #define RFM95_RST     9   // "A"
    #define RFM95_CS      10   // "B"
    #define RFM95_INT     4    // "C"
#endif

// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 915.0

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

Adafruit_Si7021 sensor = Adafruit_Si7021();

Adafruit_SH110X display = Adafruit_SH110X(64, 128, &Wire);

// OLED FeatherWing buttons map to different pins depending on board:
#if defined(ESP8266)
    #define BUTTON_A  0
    #define BUTTON_B 16
    #define BUTTON_C  2
#elif defined(ESP32)
    #define BUTTON_A 15
    #define BUTTON_B 32
    #define BUTTON_C 14
#elif defined(ARDUINO_STM32_FEATHER)
    #define BUTTON_A PA15
    #define BUTTON_B PC7
    #define BUTTON_C PC5
#elif defined(TEENSYDUINO)
    #define BUTTON_A  4
    #define BUTTON_B  3
    #define BUTTON_C  8
#elif defined(ARDUINO_NRF52832_FEATHER)
    #define BUTTON_A 31
    #define BUTTON_B 30
    #define BUTTON_C 27
#else // 32u4, M0, M4, nrf52840 and 328p
    #define BUTTON_A  9
    #define BUTTON_B  6
    #define BUTTON_C  5
#endif

unsigned long SEND_RATE = 5UL * 60UL * 1000UL;
static unsigned long LAST_SAMPLE_TIME = 0UL - SEND_RATE;

// Displays menu text
void displayMenu(String TEXT) {
    display.setCursor(0,0);
    display.clearDisplay();
    display.println(TEXT);
    display.display();
    delay(500); // Prevents double button presses
    return;
}

// Main Menu
void mainMenu() {
    static const String MAIN_MENU = "      Main Menu\n\n A: Show Temperature\n\n B: Send Sensor Data\n\n C: Configure System";
    
    displayMenu(MAIN_MENU);
    while (true) {
        if(!digitalRead(BUTTON_A)) {
            printData();
            break;
        }
        if(!digitalRead(BUTTON_B)) {
            bool DISPLAY_TEXT = false;
            int RETRY_COUNT = 0;
            sendData(DISPLAY_TEXT, RETRY_COUNT);
            break;
        }
        if(!digitalRead(BUTTON_C)) {
            configMenu();
            break;
        }
    }
}

// Config Menu
void configMenu() {
    static const String CONFIG_MENU = "  Configuration Menu\n\n A: Edit Data Rate\n\n B: ID Configuration\n\n C: Cancel";
    
    displayMenu(CONFIG_MENU);
    while (true) {
        if(!digitalRead(BUTTON_A)) { 
            msgRate();
            break;
        }
        if(!digitalRead(BUTTON_B)) {
            configID();
            break;
        }
        if(!digitalRead(BUTTON_C)) {
            delay(500);
            break;
        }
    }
}

// Message Rate
void msgRate() {
    static const String MENU1 = "   Data Send Rate\n\n A: 1 Minute\n\n B: 5 Minutes Default\n\n C: Next Page";
    static const String MENU2 = "   Data Send Rate\n\n A: 15 Minutes\n\n B: 30 Minutes\n\n C: Next Page";
    static const String MENU3 = "   Data Send Rate\n\n A: 1 Hour\n\n B: 2 Hours\n\n C: First Page";
    unsigned long MILISECONDS_IN_MINUTE = 60UL * 1000UL;
    int PAGE = 1;

    displayMenu(MENU1);
    while (true) { // Loop between pages
        if(!digitalRead(BUTTON_A)) {
            String OPTION_A = "";
            switch(PAGE) {
                case 1 :
                    SEND_RATE = 1UL * MILISECONDS_IN_MINUTE; // 1 minute
                    OPTION_A = "   Data Send Rate\n\n Set to 1 minute";
                    break;
                case 2 :
                    SEND_RATE = 15UL * MILISECONDS_IN_MINUTE; // 15 minutes
                    OPTION_A = "   Data Send Rate\n\n Set to 15 minutes";
                    break;
                case 3 :
                    SEND_RATE = 60UL * MILISECONDS_IN_MINUTE; // 1 hour - 60 minutes
                    OPTION_A = "   Data Send Rate\n\n Set to 1 hour";
                    break;
                default : 
                    OPTION_A = " Error, option does not exist";
                    break;
            }
            displayMenu(OPTION_A);
            delay(5000);
            break;
        }
        if(!digitalRead(BUTTON_B)) {
            String OPTION_B = "";
            switch(PAGE) {
                case 1 :
                    SEND_RATE = 5UL * MILISECONDS_IN_MINUTE; // 5 minutes
                    OPTION_B = "   Data Send Rate\n\n Set to 5 minutes";
                    break;
                case 2 :
                    SEND_RATE = 30UL * MILISECONDS_IN_MINUTE; // 30 minutes
                    OPTION_B = "   Data Send Rate\n\n Set to 30 minutes";
                    break;
                case 3 :
                    SEND_RATE = 120UL * MILISECONDS_IN_MINUTE; // 2 hours - 120 minutes
                    OPTION_B = "   Data Send Rate\n\n Set to 2 hours";
                    break;
                default : 
                    OPTION_B = " Error, option does not exist";
                    break;
            }
            displayMenu(OPTION_B);
            delay(5000);
            break;
        }
        if(!digitalRead(BUTTON_C)) { // Go to next page
            if (PAGE == 3) {
                PAGE = 1;
            }
            else {
                PAGE += 1;
            }
            switch(PAGE) {
                case 1 :
                    displayMenu(MENU1);
                    break;
                case 2 :
                    displayMenu(MENU2);
                    break;
                case 3 :
                    displayMenu(MENU3);
                    break;
                default :
                    String MENU_ERROR = " Error, menu page does not exist";
                    displayMenu(MENU_ERROR);
                    break;
            }
        }
    }
}

// Unique ID "configuration"
void configID() {
    static const String CONFIG_ID_MENU = "   Device ID Config\n\n A: Show ID\n\n B: Send ID\n\n C: Cancel";

    displayMenu(CONFIG_ID_MENU);
    while (true) {
        if(!digitalRead(BUTTON_A)) { // Consider using displayMenu() for this, harder to rework
            display.setCursor(0,0);
            display.clearDisplay();
            display.println("       Device ID\n");
            UniqueIDdump(Serial);
            for (size_t i = 0; i < UniqueIDsize; i++) {
                if (UniqueID[i] < 0x10)
                display.print("0");
                display.print(UniqueID[i], HEX);
                display.print(" ");
            }
            display.display();
            delay(10000);
            break;
        }
        if(!digitalRead(BUTTON_B)) {
            bool DISPLAY_TEXT = false;
            int RETRY_COUNT = 0;
            sendData(DISPLAY_TEXT, RETRY_COUNT);
            break;
        }
        if(!digitalRead(BUTTON_C)) {
            break;
        }
    }
}

// Prints sensor data to OLED
void printData() {
    display.setCursor(0,0);
    display.clearDisplay();

    display.println("     Current H/T\n"); // Consider using displayMenu() for this, harder to rework
    
    display.print(" Humidity: ");
    display.print(sensor.readHumidity(), 2);
    display.println("%\n");
    
    display.print(" Temperature: ");
    float temp = sensor.readTemperature();
    temp = (temp*1.8)+32;
    display.print(temp);
    display.println("F");
    
    display.display();
    delay(10000);
}

// Function to send data
void sendData(bool DISPLAY_TEXT, int RETRY_COUNT) {
    display.setCursor(0,0);
    display.clearDisplay();
    
    // Prepping variables
    char PACKET_ID[32];
    char PACKET_HUMIDITY[5];
    char PACKET_TEMPERATURE[5];
    char PACKET[45];
    float temp = sensor.readTemperature();
    float humid = sensor.readHumidity();
    
    UniqueIDdump(Serial);
    String ID = String("");
    for (size_t i = 0; i < UniqueIDsize; i++) {
        if (UniqueID[i] < 0x10)
            ID += ("0");
        ID += String(UniqueID[i], HEX);
    }

    // Converting temp from C to F
    temp = (temp*1.8)+32;

    // Putting all data in a char array, PACKET
    ID.toCharArray(PACKET_ID, 32);
    dtostrf(humid, 5, 2, PACKET_HUMIDITY);
    dtostrf(temp, 5, 2, PACKET_TEMPERATURE);
    strcpy(PACKET, PACKET_ID);
    strcat(PACKET, " ");
    strcat(PACKET, PACKET_HUMIDITY);
    strcat(PACKET, " ");
    strcat(PACKET, PACKET_TEMPERATURE);
    strcat(PACKET, " ");

    // Actual PACKET sending
    rf95.send((uint8_t *)PACKET, 45);

    if (DISPLAY_TEXT == false) { // Consider using displayMenu() for this, harder to rework
        // Status messages
        display.println(" Sending");
        display.print(" ID: ");
        display.println(PACKET_ID);
        display.print(" Humidity: ");
        display.println(PACKET_HUMIDITY);
        display.print(" Temperature: ");
        display.println(PACKET_TEMPERATURE);
        display.display(); 
        delay(10);
        
        // Confirm data was sent successfully
        rf95.waitPacketSent();
        sendConfirm(DISPLAY_TEXT, RETRY_COUNT);
    }
    else {
        rf95.waitPacketSent();
        sendConfirm(DISPLAY_TEXT, RETRY_COUNT);
    }
}

// Function to check if data was received
void sendConfirm(bool DISPLAY_TEXT, int RETRY_COUNT) {
    // Prepping variables
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    
    if (RETRY_COUNT >= 3) {
        return;
    }

    if (DISPLAY_TEXT == false) {
        if (rf95.waitAvailableTimeout(1000)) {
            // If reply is received and DISPLAY_TEXT is disabled   
            if (rf95.recv(buf, &len)) {
                // Receive decoding succeeds
                delay(500);
                display.print(" Got reply: ");
                display.println((char*)buf);
                display.print(" RSSI: ");
                display.println(rf95.lastRssi(), DEC);
                display.display();
                delay(5000);
            }
            else { 
                // Receive decoding fails
                delay(500);
                display.println(" Receive failed, retrying");
                display.println(" Try #"+String(RETRY_COUNT));
                display.display();
                RETRY_COUNT += 1;
                delay(3000);
                sendData(DISPLAY_TEXT, RETRY_COUNT);
            }
        }
        else {
            // No response
            delay(500);
            display.println(" No reply, retrying");
            display.println(" Try #"+String(RETRY_COUNT));
            display.display();
            RETRY_COUNT += 1;
            delay(3000);
            sendData(DISPLAY_TEXT, RETRY_COUNT);
        }
    }
    else {
        if (rf95.waitAvailableTimeout(1000)) {
            // If reply is received and DISPLAY_TEXT is enabled 
            if (rf95.recv(buf, &len)) {
                // Receive decoding succeeds
                // In this case nothing happens because it is not displaying
            }
            else {
                // Receive decoding fails
                RETRY_COUNT += 1;
                delay(3000);
                sendData(DISPLAY_TEXT, RETRY_COUNT);
            }
        }
        else {
            // No response
            RETRY_COUNT += 1;
            delay(3000);
            sendData(DISPLAY_TEXT, RETRY_COUNT);
        }
    }
    delay(500);
}

void setup() {
    display.begin(0x3C, true); // Address 0x3C default

    // Show image buffer on the display hardware.
    // Since the buffer is intialized with an Adafruit splashscreen internally, this will display the splashscreen.
    display.display();
    delay(1000);

    // Clear the buffer.
    display.clearDisplay();
    display.display();

    display.setRotation(1);

    pinMode(BUTTON_A, INPUT_PULLUP);
    pinMode(BUTTON_B, INPUT_PULLUP);
    pinMode(BUTTON_C, INPUT_PULLUP);

    // text display tests
    display.setTextSize(1);
    display.setTextColor(SH110X_WHITE);
    display.setCursor(0,0);
    display.display(); // actually display all of the above

    pinMode(RFM95_RST, OUTPUT);
    digitalWrite(RFM95_RST, HIGH);

    delay(100);

    // manual reset
    digitalWrite(RFM95_RST, LOW);
    delay(10);
    digitalWrite(RFM95_RST, HIGH);
    delay(10);

    while (!rf95.init()) {
        display.println("LoRa radio init failed");
        display.println("Uncomment '#define SERIAL_DEBUG' in RH_RF95.cpp for detailed debug info");
        display.display();
        while (1);
    }

    // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
    if (!rf95.setFrequency(RF95_FREQ)) {
        display.println("setFrequency failed");
        display.display();
        while (1);
    }
    
    rf95.setTxPower(23, false);

    display.println("Radio initialized");
    display.display();
    delay(500);

    if (!sensor.begin()) {
        display.println("Did not find Si7021 sensor!");
        display.display();
        while (true);
    }

    display.println("Sensor initialized");
    display.display();
    delay(500);
    
    display.println("Boot complete, all\ntests successful");
    display.display();
    delay(3500);
    display.clearDisplay(); 
}

void loop() {
        
    display.clearDisplay();
    display.display();

    if(!digitalRead(BUTTON_A)) { 
        mainMenu();
    }
    if(!digitalRead(BUTTON_B)) {
        mainMenu();
    }
    if(!digitalRead(BUTTON_C)) {
        mainMenu();
    }

    unsigned long now = millis();
    if (now - LAST_SAMPLE_TIME >= SEND_RATE) {
        bool DISPLAY_TEXT = true;
        int RETRY_COUNT = 0;
        LAST_SAMPLE_TIME += SEND_RATE;
        sendData(DISPLAY_TEXT, RETRY_COUNT);
    }
}
